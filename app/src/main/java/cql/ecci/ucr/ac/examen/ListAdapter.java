package cql.ecci.ucr.ac.examen;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.List;


public class ListAdapter extends ArrayAdapter<TableTop> {

    private final Activity context;
    private final List<TableTop> mGames;
    private final Integer[] imgid;


    public ListAdapter(Activity context, List<TableTop> mGames, Integer[] imgid) {
        super(context, R.layout.lista_personalizada, mGames);

        this.context = context;
        this.mGames = mGames;
        this.imgid = imgid;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.lista_personalizada, null, true);

        TextView name = (TextView) rowView.findViewById(R.id.name);
        ImageView image = (ImageView) rowView.findViewById(R.id.icon);
        TextView description = (TextView) rowView.findViewById(R.id.description);

        name.setText(mGames.get(position).getName());
        image.setImageResource(imgid[position]);
        description.setText(mGames.get(position).getDescription());

        return rowView;
    }
}
