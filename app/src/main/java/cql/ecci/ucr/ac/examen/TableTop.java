package cql.ecci.ucr.ac.examen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

public class TableTop implements Parcelable {

    private String id;
    private String name;
    private String year;
    private String publisher;
    private String country;
    private double latitude;
    private double longitude;
    private String description;
    private String numPlayers;
    private String ages;
    private String playingTime;

    //Constructor vacio
    public TableTop() {}

    //Constructor con todos los parametros
    public TableTop(String id, String name, String year, String publisher, String country, double latitude, double longitude,
                    String description, String numPlayers, String ages, String playingTime) {

        this.id = id;
        this.name = name;
        this.year = year;
        this.publisher = publisher;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.numPlayers = numPlayers;
        this.ages = ages;
        this.playingTime = playingTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumPlayers() {
        return numPlayers;
    }

    public void setNumPlayers(String numPlayers) {
        this.numPlayers = numPlayers;
    }

    public String getAges() {
        return ages;
    }

    public void setAges(String ages) {
        this.ages = ages;
    }

    public String getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(String playingTime) {
        this.playingTime = playingTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.year);
        dest.writeString(this.publisher);
        dest.writeString(this.country);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeString(this.description);
        dest.writeString(this.numPlayers);
        dest.writeString(this.ages);
        dest.writeString(this.playingTime);
    }

    protected TableTop(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.year = in.readString();
        this.publisher = in.readString();
        this.country = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.description = in.readString();
        this.numPlayers = in.readString();
        this.ages = in.readString();
        this.playingTime = in.readString();
    }

    public static final Creator<TableTop> CREATOR = new Creator<TableTop>() {
        @Override
        public TableTop createFromParcel(Parcel source) {
            return new TableTop(source);
        }

        @Override
        public TableTop[] newArray(int size) {
            return new TableTop[size];
        }
    };

    // Insertar un juego en la base de datos
    public long insert(Context context) {

        // usar la clase DataBaseHelper para realizar la operacion de insertar
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);

        // Obtiene la base de datos en modo escritura
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();

        // Crear un mapa de valores donde las columnas son las llaves
        ContentValues values = new ContentValues();

        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_ID, getId());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME, getName());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR, getYear());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER, getPublisher());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY, getCountry());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE, getLatitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE, getLongitude());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION, getDescription());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NUMPLAYERS, getNumPlayers());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES, getAges());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYINGTIME, getPlayingTime());

        // Insertar la nueva fila
        return db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP, null, values);
    }

    // Leer un juego desde la base de datos
    public void read(Context context, String id) {

        // usar la clase DataBaseHelper para realizar la operacion de leer
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);

        // Obtiene la base de datos en modo lectura
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

        // Define cuales columnas quiere solicitar
        // en este caso todas las de la clase
        String[] projection = {
                DataBaseContract.DataBaseEntry.COLUMN_NAME_ID,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_NUMPLAYERS,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYINGTIME
        };

        // Filtro para el WHERE
        String selection = DataBaseContract.DataBaseEntry.COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {id};

        // Resultados en el cursor
        Cursor cursor = db.query(
                DataBaseContract.DataBaseEntry.TABLE_NAME_TABLETOP,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        // Recorrer los resultados y asignarlos a la clase
        // aca podria implementarse un ciclo si es necesario
        if(cursor.moveToFirst() && cursor.getCount() > 0) {
            setId(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_ID)));
            setName(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_NAME)));
            setYear(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_YEAR)));
            setPublisher(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_PUBLISHER)));
            setCountry(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_COUNTRY)));
            setLatitude(Double.parseDouble(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_LATITUDE))));
            setLongitude(Double.parseDouble(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_LONGITUDE))));
            setDescription(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_DESCRIPTION)));
            setNumPlayers(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_NUMPLAYERS)));
            setAges(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_AGES)));
            setPlayingTime(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_PLAYINGTIME)));
        }
    }
}
