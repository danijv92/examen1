package cql.ecci.ucr.ac.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView list;

    private List<TableTop> mGames;

    Integer[] imgid={
            R.drawable.catan,
            R.drawable.monopoly,
            R.drawable.eldritch,
            R.drawable.mtg,
            R.drawable.hanabi,};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //inserto los datos a la base
        insertGame1();
        insertGame2();
        insertGame3();
        insertGame4();
        insertGame5();

        // Instancio y Lleno la lista
        mGames = new ArrayList<TableTop>();
        // Leo cada uno de los juegos para agregarlos a la lista
        readTableTop("TT001");
        readTableTop("TT002");
        readTableTop("TT003");
        readTableTop("TT004");
        readTableTop("TT005");

        // definimos el adaptador para la lista
        ListAdapter adapter = new ListAdapter(this, mGames, imgid);
        // Obtenemos la lista
        list = (ListView) findViewById(R.id.list);
        // asignamos el adaptador al ListView
        list.setAdapter(adapter);

        //Comportamiento cuando se clickea en una opcion
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TableTop selectedItem = (TableTop) list.getItemAtPosition(position);
                //TableTop selectedItem = (TableTop) list.getItemAtPosition(position);
                //Toast.makeText(MainActivity.this, (CharSequence) selectedItem, Toast.LENGTH_SHORT).show();

                //TableTop selectedItem = (TableTop) list.getItemAtPosition(position);
                // Metodo para llamar nueva actividad
                //goItemSelected(selectedItem);

                SelectedFragment selectedFragment = new SelectedFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("selected_game", selectedItem);
                selectedFragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.list, selectedFragment).commit();
            }
        });
    }

   /* public void goItemSelected(TableTop tableTop) {
        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(EXTRA_MESSAGE, tableTop);
        startActivityForResult(intent, SECOND_ACTIVITY_RESULT_CODE);
    }*/

    // Se insertan los datos del juego 1
    private void insertGame1() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT001",
                "Catan",
                "1995",
                "Kosmos",
                "Germany",
                48.774538,
                9.188467,
                "Picture yourself in the era of discoveries:after a long voyage of great deprivation,your ships have finally reached the coast ofan uncharted island. Its name shall be Catan!But you are not the only discoverer. Otherfearless seafarers have also landed on theshores of Catan: the race to settle theisland has begun!",
                "3-4",
                "10+",
                "1-2 hours"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Se insertan los datos del juego 2
    private void insertGame2() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT002",
                "Monopoly",
                "1935",
                "Hasbro",
                "United States",
                41.883736,
                -71.352259,
                "The thrill of bankrupting an opponent, but it pays to play nice, because fortunes could change with the roll of the dice. Experience the ups and downs by collecting property colors sets to build houses, and maybe even upgrading to a hotel!",
                "2-8",
                "8+",
                "20-180 minutes"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Se insertan los datos del juego 3
    private void insertGame3() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT003",
                "Eldritch Horror",
                "2013",
                "Fantasy Flight Games",
                "United States",
                45.015417,
                -93.183995,
                "An ancient evil is stirring. You are part ofa team of unlikely heroes engaged in an international struggle to stop the gathering darkness. To do so, you’ll have to defeat foul monsters, travel to Other Worlds, andsolve obscure mysteries surrounding thisunspeakable horror.",
                "1-8",
                "14+",
                "2-4 hours"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Se insertan los datos del juego 4
    private void insertGame4() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT004",
                "Magic: the Gathering",
                "1993",
                "Hasbro",
                "United States",
                41.883736,
                -71.352259,
                "Magic: The Gathering is a collectible and digital collectible card game created byRichard Garfield. Each game of Magic represents a battle between wizards known as planeswalkers who cast spells, use artifacts,and summon creatures.",
                "2+",
                "13+",
                "Varies"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }

    // Se insertan los datos del juego 5
    private void insertGame5() {
        // Instancia la clase TableTop y realiza la inserción de datos
        TableTop tableTop = new TableTop(
                "TT005",
                "Hanabi",
                "2010",
                "Asmodee",
                "France",
                48.761629,
                2.065296,
                "Hanabi—named for the Japanese word for\"fireworks\"—is a cooperative game in whichplayers try to create the perfect fireworks show by placing the cards on the table in theright order.",
                "2-5",
                "8+",
                "25 minutes"
        );
        // inserta el juego, se le pasa como parametro el contexto de la app
        long newRowId = tableTop.insert(getApplicationContext());
    }


    private void readTableTop(String id) {
        // Instancio el objeto
        TableTop tableTop = new TableTop();

        // Leo la fila
        tableTop.read(getApplicationContext(), id);

        // LLeno la lista de juegos
        mGames.add(tableTop);
    }
}
