package cql.ecci.ucr.ac.examen;

import android.provider.BaseColumns;

public final class DataBaseContract {

    // Para asegurar que no se instancie la clase hacemos el constructor privado
    private DataBaseContract() {}

    // Definimos una clase interna que define las tablas y columnas
    // Implementa la interfaz BaseColumns para heredar campos estandar del marco de Android _ID
    public static class DataBaseEntry implements BaseColumns {

        // Tabla
        public static final String TABLE_NAME_TABLETOP = "tableTop";

        // Columnas

        //private String id
        public static final String COLUMN_NAME_ID = "id";
        //private String name
        public static final String COLUMN_NAME_NAME = "name";
        //private String year
        public static final String COLUMN_NAME_YEAR = "year";
        //String publisher
        public static final String COLUMN_NAME_PUBLISHER = "publisher";
        //private String country
        public static final String COLUMN_NAME_COUNTRY = "country";
        //private double latitude
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        //private double longitude
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        //private String description
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        //private String numPlayers
        public static final String COLUMN_NAME_NUMPLAYERS = "numPlayers";
        //private String ages
        public static final String COLUMN_NAME_AGES = "ages";
        //private String playingTime
        public static final String COLUMN_NAME_PLAYINGTIME = "playingTime";
    }

    // Construir las tablas de la base de datos
    private static final String TEXT_TYPE = " TEXT";
    private static final String DOUBLE_TYPE = " DOUBLE";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    // Creacion de tabla
    public static final String SQL_CREATE_TABLETOP =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLETOP + " (" +
                    DataBaseEntry.COLUMN_NAME_ID + TEXT_TYPE + "PRIMARY KEY," +
                    DataBaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_YEAR + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PUBLISHER + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_COUNTRY + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LATITUDE + DOUBLE_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_LONGITUDE + DOUBLE_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_NUMPLAYERS + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_AGES + TEXT_TYPE + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_PLAYINGTIME + TEXT_TYPE + " )";

    // Borrado
    public static final String SQL_DELETE_TABLETOP =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLETOP;
}
